package Generics;


import java.util.*;

public final class Algorithm {
    public static <T> int countIf(Collection<T> c, UnaryPredicate<T> p) {

        int count = 0;
        for (T elem : c)
            if (p.test(elem))
                ++count;
        return count;
    }
    public static <T> 
    int findFirst(List<T> list, int begin, int end, UnaryPredicate<T> p) {

    for (; begin < end; ++begin)
        if (p.test(list.get(begin)))
            return begin;
    return -1;
}

// x > 0 and y > 0
public static int gcd(int x, int y) {
    for (int r; (r = x % y) != 0; x = y, y = r) { }
        return y;
}
    
}

/*public final class Algorithm {
    public static <T> T max(T x, T y) {
        return x > y ? x : y;
    }
}*/

