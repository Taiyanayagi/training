/**
 * 
 */
package Generics;

/**
 * @author taiya
 *
 */
public interface UnaryPredicate<T> {
    public boolean test(T obj);
    
}
