package training;

import java.util.function.Function;

public class DataStructure {  
    // Create an array
    private final static int SIZE = 15;
    private int[] arrayOfInts = new int[SIZE];
    
    public DataStructure() {
        // fill the array with ascending integer values
        for (int i = 0; i < SIZE; i++) {
            arrayOfInts[i] = i;
        }
    }
    
    public void printEven() {
        
        // Print out values of even indices of the array
        DataStructureIterator iterator = this.new EvenIterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
    }
    //exercie question a.
    public void print(DataStructureIterator iterator)
    {
    	while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
    }
    //Exercise question c
    public void print(java.util.function.Function<Integer, Boolean> iterator)
    {
        for (int i = 0; i < SIZE; i++) {
        	if(iterator.apply(i)) {
        		System.out.print(arrayOfInts[i] + " ");
        	}
        }
        System.out.println();
    }
    //Exercise question d
    public static Boolean isEvenIndex(Integer index) {
        if (index % 2 == 0) return Boolean.TRUE;
        return Boolean.FALSE;
    }
    
    public static Boolean isOddIndex(Integer index) {
        if (index % 2 == 0) return Boolean.FALSE;
        return Boolean.TRUE;
    }
    
    interface DataStructureIterator extends java.util.Iterator<Integer> { } 

    // Inner class implements the DataStructureIterator interface,
    // which extends the Iterator<Integer> interface
    
    private class EvenIterator implements DataStructureIterator {
        
        // Start stepping through the array from the beginning
        private int nextIndex = 0;
        
        public boolean hasNext() {
            
            // Check if the current element is the last in the array
            return (nextIndex <= SIZE - 1);
        }        
        
        public Integer next() {
            
            // Record a value of an even index of the array
            Integer retValue = Integer.valueOf(arrayOfInts[nextIndex]);
            
            // Get the next even element
            nextIndex += 2;
            return retValue;
        }
    }
    
    public static void main(String s[]) {
        
        // Fill the array with integer values and print out only
        // values of even indices
        DataStructure ds = new DataStructure();
        ds.printEven();
        //exercise question a
        EvenIterator eiobj = new DataStructure().new EvenIterator();
        ds.print(eiobj);
    	//Exercise question b - created an anonymous class DataStructureIterator and print odd values
        ds.print(new DataStructureIterator() {
            private int nextIndex = 1;
            
            public boolean hasNext() {
                
                // Check if the current element is the last in the array
                return (nextIndex <= SIZE - 1);
            }        
            
            public Integer next() {
                
                // Record a value of an even index of the array
                Integer retValue = Integer.valueOf(ds.arrayOfInts[nextIndex]);
                
                // Get the next even element
                nextIndex += 2;
                return retValue;
            }
        });
      //Exercise question c using lambda
        ds.print(new Function<Integer, Boolean>() {
			
			@Override
			public Boolean apply(Integer t) {
				// TODO Auto-generated method stub
				return t%2==1;
			}
		});
        Function<Integer, Boolean> isOdd= (Integer val)-> {return val%2==1;};
        Function<Integer, Boolean> isEven= (Integer val)-> {return val%2==0;};
        
        ds.print(isOdd);
        ds.print(isEven);
    //Exercise question d
        ds.print(DataStructure::isEvenIndex);
        ds.print(DataStructure::isOddIndex);
        
        
    }
}