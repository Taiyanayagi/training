package training;

public class Deck2 {
	private static card2[] cards = new card2[52];
    public Deck2() {
        int i = 0;
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                cards[i++] = new card2(rank, suit);
            }
        }
    }
}