package training;

public interface IAccount {

	public Integer getBalance();
	public boolean creditAccount(Integer creditValue);
	public boolean debitAccount(Integer debitValue);
}
