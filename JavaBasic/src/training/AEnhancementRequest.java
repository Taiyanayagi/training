package training;

public @interface AEnhancementRequest {
int id();
String synopsis();
String date() default "[unassigned]";
String engineer() default "[unknown]";
}
