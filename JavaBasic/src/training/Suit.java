package training;

public enum Suit {
	DIAMONDS, 
	   CLUBS, 
	   HEARTS, 
	   SPADES
}
