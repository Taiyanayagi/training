package training;

import java.text.DecimalFormat;

public class FPAdder {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int numofargs=args.length;
		double sum=0.0;
		if (numofargs<2)
		{
			System.out.println("need to enter min 2 commandline value");
		}
		
		else
		{
		for(int i=0;i<numofargs;i++)
		{
			sum+=Double.valueOf(args[i]).doubleValue();
		}
		//format the sum
	    DecimalFormat myFormatter = new DecimalFormat("###,###.##");
	    String output = myFormatter.format(sum);
		System.out.println("sum = "+output);
		}
	}

}