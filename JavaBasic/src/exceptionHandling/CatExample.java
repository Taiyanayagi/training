package exceptionHandling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class CatExample {
	
	public static void cat(File file) {
		
	    RandomAccessFile input = null;
	    String line = null;
	    try
		{
	        input = new RandomAccessFile(file, "r");
	        while ((line = input.readLine()) != null) {
	            System.out.println(line);
	        }
	        return;
	    }
	    catch(FileNotFoundException fnf) {
	        System.err.format("File: %s not found%n", file);
	    } catch(IOException e) {
	        System.err.println(e.toString());
	    }
		finally {
	        if (input != null) {
	            try {
					input.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.err.println(e.toString());
				}
	        }
	    }
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CatExample catobj=new CatExample();
		File myfile= new File("infile.txt");
				
		catobj.cat(myfile);

	}

}
