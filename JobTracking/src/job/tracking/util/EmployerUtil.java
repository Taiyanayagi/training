package job.tracking.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import job.tracking.model.Employee;
import job.tracking.model.Employer;

public class EmployerUtil {
	public static Function<Employee, String> printName= (Employee val)-> {return val.getName();};
	public static Function<Employee, String> printNameAndSalary= (Employee val)-> {return String.format("%20s%15.02f", val.getName(), val.getTotalSalary());};

	public static void saveJobDetails(Collection<Employer> employerList, Collection<Employee> unemployedPeople, String fileName) {
		OutputStream fileOutputStream = FileUtil.getFileOutputStream(fileName);

		if (fileOutputStream != null) {
			for (Employer employer : employerList) {
				printJobDetails(employer, fileOutputStream, printNameAndSalary);
			}
			try {
				fileOutputStream.write("Unemployed People:".getBytes());
				fileOutputStream.write(System.getProperty("line.separator").getBytes());
				printEmployees(unemployedPeople, System.out, printNameAndSalary);
				fileOutputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static Map<String, Employer> getEmployerMap(List<Employer> employerList) {
		Map<String, Employer> employerMap = new HashMap<>();
		for (Employer employer : employerList) {
			employerMap.put(employer.getName(), employer);
		}
		return employerMap;
	}

	public static void printJobDetails(Employer emp, OutputStream out) {
		printJobDetails(emp, out, printName);
	}
	public static void printJobDetails(Employer emp, OutputStream out, Function<Employee, String> printEmployee) {
		try {
			if (emp == null) {
				out.write("Company not found for the given name.".getBytes());
				return;
			}
			out.write(("Company name:" + emp.getName()).getBytes());
			out.write(System.getProperty("line.separator").getBytes());
			if (emp.getEmployees().size() > 0) {
				out.write("Employees:".getBytes());
				out.write(System.getProperty("line.separator").getBytes());
				Collections.sort(emp.getEmployees(), (e1, e2) -> (e1.getName().compareTo(e2.getName())));
				printEmployees(emp.getEmployees(), out, printEmployee);
			} else {
				out.write("No employees.".getBytes());
				out.write(System.getProperty("line.separator").getBytes());
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void printEmployees(List<Employee> employees, OutputStream out) {
		printEmployees(employees, out, printName); 
	}
	public static void printEmployees(Collection<Employee> employees, OutputStream out, Function<Employee, String> printEmployee) {
		try {
			for (Employee employee : employees) {
				out.write((printEmployee.apply(employee)).getBytes());
				out.write(System.getProperty("line.separator").getBytes());
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static Employee getEmployee(String employeeName, Map<String, Employee> employeeMap) {
		Employee currentEmp = employeeMap.get(employeeName);
		if (currentEmp == null) {
			currentEmp = new Employee();
			currentEmp.setName(employeeName);
			employeeMap.put(employeeName, currentEmp);
		}
		return currentEmp;
	}
}
