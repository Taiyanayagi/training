package job.tracking.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import job.tracking.model.Employer;

public class FileUtil {

	public static OutputStream getFileOutputStream(String fileName) {
		File file = new File(fileName);

		try {
			return new BufferedOutputStream(new FileOutputStream(file));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static List<Employer> getEmployersFromFile(String fileName) {
		List<Employer> employers = new ArrayList<>();
		String companyName = null;
		try {
			InputStream in = FileUtil.class.getClassLoader().getResourceAsStream(fileName);
			BufferedReader buffer = new BufferedReader(new InputStreamReader(in));
			while ((companyName = buffer.readLine()) != null) {
				if (!companyName.isEmpty()) {
					if (companyName.length() > 10) {
						System.out.println("Ignoring the company name " + companyName + " with size more than 10.");
					} else {
						Employer emp = new Employer();
						emp.setName(companyName);
						employers.add(emp);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return employers;
	}
}
