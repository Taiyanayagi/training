package job.tracking;

import java.util.Scanner;

import job.tracking.application.TrackingSystem;

public class JobTrackerExecuter {
	public static void main(String[] args) {
		TrackingSystem tracker = new TrackingSystem();
		Scanner scan = new Scanner(System.in);
		String inputLine = null;
		while ((inputLine = scan.nextLine()) != null) {
			String[] commandParams = inputLine.split(" ");
			if (commandParams.length == 0) {
				continue;
			}
			switch (commandParams[0]) {
			case "JOIN":
				if(commandParams.length < 3) {
					System.out.println("Please enter the employee and company name to join the employee.");
					break;
				}
				tracker.processJoinEmployee(commandParams[1], commandParams[2]);
				System.out.println("Joined the employee.");
				break;
			case "CHANGE":
				if(commandParams.length < 3) {
					System.out.println("Please enter the employee and company name to change the employee.");
					break;
				}
				System.out.println("Changed the employee.");
				tracker.processChangeEmployee(commandParams[1], commandParams[2]);
				break;
			case "QUIT":
				if(commandParams.length < 2) {
					System.out.println("Please add the employee name to quit the employee.");
					break;
				}
				tracker.processQuitEmployee(commandParams[1]);
				System.out.println("Quited the employee.");
				break;
			case "PAYDAY":
				tracker.processPayDay();
				System.out.println("Salary paid to all the employees.");
				break;
			case "EMPLOYEES":
				if(commandParams.length < 2) {
					System.out.println("Please add the company name to print the employees of it.");
					break;
				}
				tracker.printCompanyEmployees(commandParams[1]);
				break;
			case "UNEMPLOYED":
				tracker.printUnemployedEmployees();
				break;
			case "DUMP":
				
				tracker.processDump();
				break;
			case "END":
				tracker.saveJobDetails();
				scan.close();
				System.exit(0);
			default:
				continue;

			}
		}
	}

}
