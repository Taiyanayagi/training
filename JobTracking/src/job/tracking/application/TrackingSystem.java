package job.tracking.application;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import job.tracking.model.Employee;
import job.tracking.model.Employer;
import job.tracking.util.EmployerUtil;
import job.tracking.util.FileUtil;

public class TrackingSystem implements IJobTrackingSystem {

	public static final BigDecimal UNEMPLOYED_SALARY = new BigDecimal(700.00);
	public static final BigDecimal EMPLOYED_SALARY = new BigDecimal(10000.00);

	private Map<String, Employer> employerMap = null;
	private Map<String, Employee> employeeMap = new HashMap<>();
	private List<Employee> unemployedEmployees = new ArrayList<>();

	public TrackingSystem() {
		List<Employer> employers = FileUtil.getEmployersFromFile("company.txt");
		employerMap = EmployerUtil.getEmployerMap(employers);
	}
	public void printCompanyEmployees(String companyName) {
		Employer emp = employerMap.get(companyName);
		EmployerUtil.printJobDetails(emp, System.out);
	}

	public void processPayDay() {
		for (Employee employee : employeeMap.values()) {
			employee.addSalary(employee.getEmployer() != null?EMPLOYED_SALARY:UNEMPLOYED_SALARY);
		}

	}

	public void processJoinEmployee(String employeeName, String employerName) {
		Employee currentEmp = EmployerUtil.getEmployee(employeeName, employeeMap);
		if(currentEmp.getEmployer() != null) {
			System.out.println("This employee is already working in Company " + currentEmp.getEmployer().getName());
			return;
		}
		Employer currentEmployer = employerMap.get(employerName);
		if(currentEmployer == null) {
			System.out.println("Employer not available.");
			return;
		}
		currentEmp.setEmployer(currentEmployer);
		currentEmployer.getEmployees().add(currentEmp);
		if(unemployedEmployees.contains(currentEmp)) {
			unemployedEmployees.remove(currentEmp);
		}
	}

	public void processChangeEmployee(String employeeName, String employerName) {
		Employee currentEmp = EmployerUtil.getEmployee(employeeName, employeeMap);
		if(currentEmp.getEmployer() == null) {
			System.out.println("This employee is not working in any Company ");
			return;
		}
		Employer currentEmployer = employerMap.get(employerName);
		if(currentEmployer == null) {
			System.out.println("Employer not available.");
			return;
		}
		currentEmp.getEmployer().getEmployees().remove(currentEmp);
		currentEmp.setEmployer(currentEmployer);
		currentEmployer.getEmployees().add(currentEmp);
	}

	public void processQuitEmployee(String employeeName) {
		Employee currentEmp = EmployerUtil.getEmployee(employeeName, employeeMap);
		if(currentEmp.getEmployer() == null) {
			System.out.println("This employee is not working in any Company ");
			return;
		}
		currentEmp.getEmployer().getEmployees().remove(currentEmp);
		currentEmp.setEmployer(null);
		unemployedEmployees.add(currentEmp);
	}

	public void printUnemployedEmployees() {
		EmployerUtil.printEmployees(unemployedEmployees, System.out);
	}

	public void processDump() {
		System.out.println("Assignment: Job Tracking System");
		for (Employer employer : employerMap.values()) {
			EmployerUtil.printJobDetails(employer, System.out);
		}
		System.out.println("Unemployed People:");
		printUnemployedEmployees();
	}
	public void saveJobDetails() {
		EmployerUtil.saveJobDetails(employerMap.values(), unemployedEmployees, "employee.txt");

	}
}
