package job.tracking.application;

public interface IJobTrackingSystem {

	public void printCompanyEmployees(String companyName);
	public void processPayDay();
	public void processJoinEmployee(String employeeName, String employerName);
	public void processChangeEmployee(String employeeName, String employerName);
	public void processQuitEmployee(String employeeName);
	public void printUnemployedEmployees();
	public void processDump();
	public void saveJobDetails();
	
}
