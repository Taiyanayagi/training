package job.tracking.model;

import java.util.ArrayList;
import java.util.List;

public class Employer {
	String name;
	List<Employee> employees;
	public Employer() {
		employees = new ArrayList<>();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Employee> getEmployees() {
		return employees;
	}
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

}
