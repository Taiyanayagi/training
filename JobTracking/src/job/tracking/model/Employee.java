package job.tracking.model;

import java.math.BigDecimal;

public class Employee {
	String name;
	Employer employer;
	BigDecimal salary;
	BigDecimal totalSalary;
	public Employee() {
		totalSalary = new BigDecimal(0);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Employer getEmployer() {
		return employer;
	}
	public void setEmployer(Employer employer) {
		this.employer = employer;
	}
	public BigDecimal getSalary() {
		return salary;
	}
	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}
	public BigDecimal getTotalSalary() {
		return totalSalary;
	}
	public void setTotalSalary(BigDecimal totalSalary) {
		this.totalSalary = totalSalary;
	}

	public void addSalary(BigDecimal salary) {
		this.totalSalary = this.totalSalary.add(salary);
	}
}
